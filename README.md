# posts-app

## Project setup
```
npm install
```

### Compiles, hot-reloads for development and start a local proxy at port 3000
```
npm run start
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
