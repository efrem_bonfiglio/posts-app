const express = require('express');
const proxy = require('express-http-proxy');
const app = express();

const PORT = 3000;
const ORIGIN = 'https://jsonplaceholder.typicode.com';

const getColor = (color) => {
	let colorCode = '\x1b[0m\x1b[1m';

	switch (color) {
		case 'green':
			colorCode = '\x1b[32m\x1b[1m';
			break;
		case 'red':
			colorCode = '\x1b[31m\x1b[1m';
			break;
		case 'yellow':
			colorCode = '\x1b[33m\x1b[1m';
			break;
		case 'magenta':
			colorCode = '\x1b[35m\x1b[1m';
			break;
		default: break;
	}

	return colorCode;
};

app.use('/', proxy(ORIGIN, {
	proxyReqPathResolver: function (req) {
		return req.url;
	}
}));

app.listen(PORT, () => console.log(`${getColor('yellow')} Proxy listening on port ${PORT}`));
