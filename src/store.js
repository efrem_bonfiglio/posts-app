import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';

import types from './mutation-types';

Vue.use(Vuex);

const API_HOST = 'http://localhost:3000';

const getPostIndex = (posts, postId) => {
	let index = -1;
	posts.forEach((pst, i) => {
		if (pst.id === postId) {
			index = i;
		}
	});
	return index;
};

export default new Vuex.Store({
	state: {
		posts: [],
		loading: false
	},
	getters: {
		getPostById: state => id => {
			return state.posts.filter(post => post.id === id)[0]
		}
	},
	mutations: {
		[types.API_REQUEST](state) {
			state.loading = true;
		},
		[types.API_SUCCESS](state) {
			state.loading = false;
		},
		[types.API_FAILURE](state) {
			state.loading = false;
			// TODO: Replace with a modal component
			alert('Si è verificato un errore');
		},
		[types.ADD_POSTS](state, payload) {
			if (payload.index) {
				// If index is provided, remove old post and push th new one
				state.posts.splice(payload.index, 1, ...payload.posts);
			} else {
				// Add the new post at the end of the list
				state.posts = [...state.posts, ...payload.posts];
			}
		},
		[types.REMOVE_POST](state, payload) {
			state.posts.splice(payload.index, 1);
		}
	},
	actions: {
		fetchPosts({commit}) {
			commit(types.API_REQUEST);
			axios
				.get(`${API_HOST}/posts`)
				.then(res => {
					commit(types.ADD_POSTS, {posts: res.data});
					commit(types.API_SUCCESS);
				})
				.catch(() => {
					commit(types.API_FAILURE);
				});
		},
		editPost({commit, state}, post) {
			commit(types.API_REQUEST);
			axios
				.put(`${API_HOST}/posts/${post.id}`, post)
				.then(() => {
					commit(types.ADD_POSTS, {posts: [post], index: getPostIndex(state.posts, post.id)});
					commit(types.API_SUCCESS);
				})
				.catch(() => {
					commit(types.API_FAILURE);
				});
		},
		addPost({commit}, post) {
			commit(types.API_REQUEST);
			axios
				.post(`${API_HOST}/posts`, post)
				.then(res => {
					commit(types.ADD_POSTS, {posts: [res.data]});
					commit(types.API_SUCCESS);
				})
				.catch(() => {
					commit(types.API_FAILURE);
				});
		},
		removePost({commit, state}, id) {
			commit(types.API_REQUEST);
			axios
				.delete(`${API_HOST}/posts/${id}`)
				.then(() => {
					commit(types.REMOVE_POST, {index: getPostIndex(state.posts, id)});
					commit(types.API_SUCCESS);
				})
				.catch(() => {
					commit(types.API_FAILURE);
				});
		}
	}
});
