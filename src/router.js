import Vue from 'vue';
import VueRouter from 'vue-router';

import EditPost from './components/EditPost';
import CreatePost from './components/CreatePost';
import store from "./store";

Vue.use(VueRouter);

const routes = [
	{
		name: 'edit-post',
		path: '/edit-post/:id',
		component: EditPost,
		beforeEnter: (to, from, next) => {
			// You can only change a post if posts array exists
			if (store.state.posts.length) {
				next();
			} else {
				next('/');
			}
		}
	},
	{
		path: '*',
		component: CreatePost
	}
];

export default new VueRouter({
	routes
});
